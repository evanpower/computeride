use std::fs;

extern crate argparse;
use argparse::{ArgumentParser, Store};

extern crate gpx_parse;
use gpx_parse::{Point, gpx_parse_points_elv_time_speed};

extern crate gpx_vis;
use gpx_vis::ascii::elv_graph;

extern crate gpx_stats;
use gpx_stats::{
    distance_km,
    average_speed_kmph,
    max_speed_kmph,
    elapsed_time_seconds,
    moving_time_seconds };

extern crate segment_matcher;
use segment_matcher::SegmentMatcher;

const SEGMENTS_PATH: &str = "./data/segments/";
const DISTANCE_THRESHOLD_DEGREES: f64 = 0.0002;
struct Arguments {
    track_file_path: String
}

fn main() {
    let mut args = Arguments {
        track_file_path: "".to_string(),
    };
    {
        let mut parser = ArgumentParser::new();
        parser.set_description("Analyzes a GPS track or something.");
        parser.refer(&mut args.track_file_path)
            .required()
            .add_argument("Track-File-Path", Store, "Path to the GPX file describing the track");
        parser.parse_args_or_exit();
    }

    // Import a gpx file.
    let points = gpx_parse_points_elv_time_speed(&(args.track_file_path));

    // Let's start by making a cool elevation map.
    elv_graph(&points);

    // Let's calculate the distance travelled.
    let dist = distance_km(&points);
    println!("Distance: {} km", dist);

    // Let's calculate the average speed.
    let speed = average_speed_kmph(&points);
    println!("Average speed: {} km/h", speed);

    // Max speed
    let max_speed = max_speed_kmph(&points);
    println!("Max speed: {} km/h", max_speed);

    // Elapsed time
    let elapsed_time = elapsed_time_seconds(&points);
    println!("Elapsed time: {}", seconds_to_string(elapsed_time));

    // Moving time
    let moving_time = moving_time_seconds(&points);
    println!("Moving time: {}", seconds_to_string(moving_time));

    // Climb

    // Get a list of segments in the segment folder.
    // Compare the current segment to each of them.
    match_segments(&points);

}

fn seconds_to_string(seconds: i32) -> String {
    let hr: i32 = seconds / 3600;
    let min: i32 = (seconds % 3600) / 60;
    let sec: i32 = seconds % 60;
    let mut ret = "".to_owned();
    if seconds == 0 {
        return "None".to_owned();
    }
    if hr != 0 {
        ret.push_str(&(hr.to_string()));
        ret.push_str(" hours ");

    }
    if min != 0 {
        ret.push_str(&(min.to_string()));
        ret.push_str(" minutes ");
    }
    if sec != 0 {
        ret.push_str(&(sec.to_string()));
        ret.push_str(" seconds ");
    }
    return ret.to_owned();
}

fn match_segments(points: &Vec<Point>) {
    //Get a list of files in the segments folder.
    let paths = fs::read_dir(SEGMENTS_PATH).unwrap();

    for path in paths {
        let filename: String = path.unwrap().path().into_os_string().into_string().unwrap();
        if filename.to_lowercase().ends_with(".gpx")  {
            //println!("Comparing track to segment: {}", filename);
            let segment = gpx_parse_points_elv_time_speed(&filename);
            let seg_matcher: SegmentMatcher = SegmentMatcher { segment: &segment };
            let result = seg_matcher.check_track(points);
            if result.possible_match == true && result.mean_distance < DISTANCE_THRESHOLD_DEGREES {
                println!("You rode {}", &filename);
                //println!("Possible match");
                //println!("Mean distance: {}", result.mean_distance)
            }
        }

    }
}
